﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnerNotification.ScheduleDeliveryNotification
{
    public class ScheduleDeliveryNotificationModel
    {
        public string OrderNo;
        public string DeliveryDate;
        public string AvailableDeliveryDate;
        public string SaleDate;
        public string CustomerId;
        public string ContactNo1;
        public string ContactNo2;
        public string Email;
        public string TemplateId;
        //public string DeliveryCode;
        //public string DurationDay;
        public string Purpose;
        public string Type;
        public string Message;
        public string Status1;
        public string Status2;
        public string Id;
        public string CompanyId;
        public string StoreId;
        public string CreatedBy;
    }
}
