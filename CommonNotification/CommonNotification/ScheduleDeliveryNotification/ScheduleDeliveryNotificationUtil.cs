﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using TurnerNotification.CommonUtil;

namespace TurnerNotification.ScheduleDeliveryNotification
{
    public static class ScheduleDeliveryNotificationUtil
    {
        public static bool SenNotification()
        {
            bool successs = false;
            //get notification infomation
            DataTable dt = new DataTable();
            ScheduleDeliveryNotificationModel orderDeliveryDetails = new ScheduleDeliveryNotificationModel();

            int counter = 0;
            try
            {
                string client_app = ConfigurationManager.AppSettings["client_app"];
                string notification_jobtype = string.Empty;
                notification_jobtype = ConfigurationManager.AppSettings["notification_jobtype"];
                
                if (client_app == "turner")
                {
                    dt = ScheduleDeliveryNotificationSQLHelper.GetSplitCompletedNotifictionDetails(notification_jobtype);
                }
                else if (client_app == "trivetts")
                {
                    //dt = ScheduleDeliveryNotificationSQLHelper.GetNotifictionDetails();
                    dt = ScheduleDeliveryNotificationSQLHelper.GetSplitCompletedNotifictionDetails(notification_jobtype);
                }
                else if (client_app == "twg")
                {
                    dt = ScheduleDeliveryNotificationSQLHelper.GetSplitCompletedNotifictionDetails(notification_jobtype);
                }


                if (dt.Rows.Count > 0)
                {
                    string purpose = string.Empty;
                    string createdby = string.Empty;
                    
                    string url = ConfigurationManager.AppSettings["redirecturl"];
                    string key_link = string.Empty;
                    string body = string.Empty;

                    DataRow[] rows = dt.Select();

                    if (client_app == "turner")
                    {
                        #region turner
                        if (notification_jobtype=="1")//completed
                        {
                            var result = dt.AsEnumerable().Where(dr => dr.Field<string>("Items In Stock") == "In Stock");
                            DataTable dt1 = result.CopyToDataTable();
                            rows = dt1.Select();
                            ///

                            purpose = "Complete Delivery";
                            createdby = "Complete Delivery Service";
                            key_link = ConfigurationManager.AppSettings["key_trcdel"];
                            body = "Hi, this is your Ashley HomeStore.  \nGood news, we are ready to schedule you for delivery of your purchase!  Please click here to confirm your delivery date.  ";
                        }
                        else if(notification_jobtype == "2")//split
                        {
                            var result = dt.AsEnumerable().Where(dr => dr.Field<string>("Items In Stock") != "In Stock");
                            DataTable dt1 = result.CopyToDataTable();
                            rows = dt1.Select();

                            purpose = "Split Delivery";
                            createdby = "Split Delivery Service";
                            key_link = ConfigurationManager.AppSettings["key_trsdel"];
                            body = "Hi, this is your Ashley HomeStore. \nGood news,  we can offer you a partial delivery of your purchase (at no extra cost to you!).  Please click here to confirm your delivery date. ";
                        }
                        #endregion

                    }
                    else if (client_app == "trivetts")
                    {
                        //purpose = "ReSchedule Delivery";
                        //createdby = "ReSchedule Delivery Service";

                        //key_link = ConfigurationManager.AppSettings["key_sched"];
                        //body = "Hi, \nThis is your Ashley Homestore.  \nPlease confirm your delivery date using this link  ";
                        if (notification_jobtype == "1")//completed
                        {
                            var result = dt.AsEnumerable().Where(dr => dr.Field<string>("Items In Stock") == "In Stock");
                            DataTable dt1 = result.CopyToDataTable();
                            rows = dt1.Select();
                            ///

                            purpose = "Complete Delivery";
                            createdby = "Complete Delivery Service";
                            key_link = ConfigurationManager.AppSettings["key_tvcdel"];
                            body = "Hi, this is your Ashley HomeStore.  \nGood news, we are ready to schedule you for delivery of your purchase!  Please click here to confirm your delivery date.  ";
                        }
                        else if (notification_jobtype == "2")//split
                        {
                            var result = dt.AsEnumerable().Where(dr => dr.Field<string>("Items In Stock") != "In Stock");
                            DataTable dt1 = result.CopyToDataTable();
                            rows = dt1.Select();

                            purpose = "Split Delivery";
                            createdby = "Split Delivery Service";
                            key_link = ConfigurationManager.AppSettings["key_tvsdel"];
                            body = "Hi, this is your Ashley HomeStore. \nGood news,  we can offer you a partial delivery of your purchase (at no extra cost to you!).  Please click here to confirm your delivery date. ";
                        }
                    }
                    else if (client_app == "twg")
                    {
                        #region twg
                        if (notification_jobtype == "1")//completed
                        {
                            var result = dt.AsEnumerable().Where(dr => dr.Field<string>("Items In Stock") == "In Stock");
                            DataTable dt1 = result.CopyToDataTable();
                            rows = dt1.Select();
                            ///

                            purpose = "Complete Delivery";
                            createdby = "Complete Delivery Service";
                            key_link = ConfigurationManager.AppSettings["key_twcdel"];
                            body = "Hi, this is your Ashley HomeStore.  \nGood news, we are ready to schedule you for delivery of your purchase!  Please click here to confirm your delivery date.  ";
                        }
                        else if (notification_jobtype == "2")//split
                        {
                            var result = dt.AsEnumerable().Where(dr => dr.Field<string>("Items In Stock") != "In Stock");
                            DataTable dt1 = result.CopyToDataTable();
                            rows = dt1.Select();

                            purpose = "Split Delivery";
                            createdby = "Split Delivery Service";
                            key_link = ConfigurationManager.AppSettings["key_twsdel"];
                            body = "Hi, this is your Ashley HomeStore. \nGood news,  we can offer you a partial delivery of your purchase (at no extra cost to you!).  Please click here to confirm your delivery date. ";
                        }
                        #endregion
                    }

                    url = string.Format("{0}/{1}/", url, key_link);

                    foreach (DataRow row in rows )
                    {

                        //if (row["so_no"].ToString() != "999147630" && row["so_no"].ToString() != "10647260")//"4776380")
                       // if(row["so_no"].ToString().Contains(','))
                        { //|| row["so_no"].ToString() == "4788370"
                        if (counter >=1)
                            break;

                            orderDeliveryDetails = new ScheduleDeliveryNotificationModel();

                            orderDeliveryDetails.Purpose = purpose;
                            orderDeliveryDetails.CreatedBy = createdby;
                            orderDeliveryDetails.Type = "SMS";

                            if (client_app == "turner")
                            {
                                #region turner
                                orderDeliveryDetails.OrderNo = row["Sales Number"].ToString().Trim();
                                orderDeliveryDetails.CustomerId = row["Cust_id"].ToString();
                                orderDeliveryDetails.Email =null;
                                orderDeliveryDetails.DeliveryDate = row["deliverydate"].ToString();
                                orderDeliveryDetails.CompanyId = row["company_id"].ToString();
                                orderDeliveryDetails.StoreId = row["profitcenter_id"].ToString();

                                string phone = row["phone 1"].ToString();
                                string[] arrphone = phone.Split(',');
                                if(arrphone != null && arrphone.Length>1)
                                {
                                    orderDeliveryDetails.ContactNo1 = arrphone[0].Trim();
                                    orderDeliveryDetails.ContactNo2 = arrphone[1].Trim();
                                }
                                else
                                {
                                    orderDeliveryDetails.ContactNo1 = phone.Trim();
                                    orderDeliveryDetails.ContactNo2 = "";
                                }
                                #endregion

                            }
                            else if (client_app == "trivetts")
                            {
                                #region trivetts

                                //old code
                                //orderDeliveryDetails.OrderNo = row["so_no"].ToString().Trim();
                                //orderDeliveryDetails.CustomerId = row["so_cust_id"].ToString();
                                //orderDeliveryDetails.Email = row["cust_email"].ToString();
                                //orderDeliveryDetails.DeliveryDate = row["delivery_date"].ToString();
                                //orderDeliveryDetails.AvailableDeliveryDate = row["avail_del_date"].ToString();
                                //orderDeliveryDetails.ContactNo1 = row["cust_phone_no"].ToString();
                                //orderDeliveryDetails.ContactNo2 = row["cust_phone_no_2"].ToString();
                                orderDeliveryDetails.OrderNo = row["Sales Number"].ToString().Trim();
                                orderDeliveryDetails.CustomerId = row["Cust_id"].ToString();
                                orderDeliveryDetails.Email = null;
                                orderDeliveryDetails.DeliveryDate = row["deliverydate"].ToString();
                                orderDeliveryDetails.CompanyId = row["company_id"].ToString();
                                orderDeliveryDetails.StoreId = row["profitcenter_id"].ToString();

                                string phone = row["phone 1"].ToString();
                                string[] arrphone = phone.Split(',');
                                if (arrphone != null && arrphone.Length > 1)
                                {
                                    orderDeliveryDetails.ContactNo1 = arrphone[0].Trim();
                                    orderDeliveryDetails.ContactNo2 = arrphone[1].Trim();
                                }
                                else
                                {
                                    orderDeliveryDetails.ContactNo1 = phone.Trim();
                                    orderDeliveryDetails.ContactNo2 = "";
                                }

                                #endregion
                            }
                            else if (client_app == "twg")
                            {
                                #region twg
                                //orderDeliveryDetails.OrderNo = row["so_no"].ToString().Trim();
                                //orderDeliveryDetails.CustomerId = row["so_cust_id"].ToString();
                                //orderDeliveryDetails.Email = row["cust_email"].ToString();
                                //orderDeliveryDetails.DeliveryDate = row["delivery_date"].ToString();
                                //orderDeliveryDetails.AvailableDeliveryDate = row["avail_del_date"].ToString();
                                //orderDeliveryDetails.ContactNo1 = row["cust_phone_no"].ToString();
                                //orderDeliveryDetails.ContactNo2 = row["cust_phone_no_2"].ToString();
                                orderDeliveryDetails.OrderNo = row["Sales Number"].ToString().Trim();
                                orderDeliveryDetails.CustomerId = row["Cust_id"].ToString();
                                orderDeliveryDetails.Email = null;
                                orderDeliveryDetails.DeliveryDate = row["deliverydate"].ToString();
                                orderDeliveryDetails.CompanyId = row["company_id"].ToString();
                                orderDeliveryDetails.StoreId = row["profitcenter_id"].ToString();

                                string phone = row["phone 1"].ToString();
                                string[] arrphone = phone.Split(',');
                                if (arrphone != null && arrphone.Length > 1)
                                {
                                    orderDeliveryDetails.ContactNo1 = arrphone[0].Trim();
                                    orderDeliveryDetails.ContactNo2 = arrphone[1].Trim();
                                }
                                else
                                {
                                    orderDeliveryDetails.ContactNo1 = phone.Trim();
                                    orderDeliveryDetails.ContactNo2 = "";
                                }
                                #endregion
                            }

                            //if(counter<=1)
                            //{
                            //        orderDeliveryDetails.ContactNo1 = "5404195311";// "+919860645687";//"703-899-9630";///
                            //    orderDeliveryDetails.ContactNo2 = "";
                            //}
                            //else
                            //{
                            // orderDeliveryDetails.ContactNo1 = "703-899-9630";// "5404195311";// " +919831303330";// "703-899-9630";// "5408346910";//"+919831303330"; //"+919831303330"; ;//
                            // orderDeliveryDetails.ContactNo2 = "";
                            //}
                            //orderDeliveryDetails.ContactNo1 = "703-899-9630"; //"5404195311 "--//"7038999630";// "+919860645687";
                            //orderDeliveryDetails.ContactNo2 = "+919860645687";//"5408346910"--//"2292244765";// "+917276092022";
                            //5404195311


                            string id = string.Empty;
                            id = ScheduleDeliveryNotificationSQLHelper.InsertScheduleDeliverySmsStatus(orderDeliveryDetails);
                            if (string.IsNullOrWhiteSpace(id))
                                continue;

                            orderDeliveryDetails.Id = id;
                            orderDeliveryDetails.Message = string.Format("{0}{1}{2} ", body, url, id);

                            #region send sms
                            //sms - contact1
                            if (!string.IsNullOrWhiteSpace(orderDeliveryDetails.ContactNo1))
                            {
                                try
                                {
                                    orderDeliveryDetails.ContactNo1 = string.Format("+1{0}", orderDeliveryDetails.ContactNo1.Trim());

                                    //successs = SMSUtil.SendSMSNotification(orderDeliveryDetails.ContactNo1, orderDeliveryDetails.Message);

                                    if (successs)
                                        orderDeliveryDetails.Status1 = "Success";
                                    else
                                        orderDeliveryDetails.Status1 = "Failed";

                                }
                                catch (Exception ex)
                                {
                                    successs = false;
                                    orderDeliveryDetails.Status1 = string.Format("Failed - {0}", ex.Message);
                                }

                            }

                            //sms - contact2
                            if (!string.IsNullOrWhiteSpace(orderDeliveryDetails.ContactNo2))
                            {
                                try
                                {
                                    orderDeliveryDetails.ContactNo2 = string.Format("+1{0}", orderDeliveryDetails.ContactNo2.Trim());
                                    //successs = SMSUtil.SendSMSNotification(orderDeliveryDetails.ContactNo2, orderDeliveryDetails.Message);

                                    if (successs)
                                        orderDeliveryDetails.Status2 = "Success";
                                    else
                                        orderDeliveryDetails.Status2 = "Failed";

                                }
                                catch (Exception ex)
                                {
                                    successs = false;
                                    orderDeliveryDetails.Status2 = string.Format("Failed - {0}", ex.Message);
                                }
                            }
                            #endregion

                            //update InsertDeliverySmsStatus  
                            if (string.IsNullOrWhiteSpace(orderDeliveryDetails.ContactNo1) && string.IsNullOrWhiteSpace(orderDeliveryDetails.ContactNo2))
                                orderDeliveryDetails.Status1 = "Failed - No any contact number available.";

                            ScheduleDeliveryNotificationSQLHelper.UpdateCalllogDelivery(orderDeliveryDetails);
                            //counter++;
                            }

                    }
                }

            }
            catch(Exception e) {

            }

            return successs;
        }

    }
}
