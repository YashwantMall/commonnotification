﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace TurnerNotification
{
    public class Program
    {
        //Declare an instance for log4net
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            //TODO: set test scheduler args
            args = new string[1];
            args[0] = ConfigurationManager.AppSettings["ProcessType"];

            if (args.Length <= 0)
            {
                Console.WriteLine("Scheduler argument error: No argument found.");
                Log.Error("Scheduler argument error: No argument found.");
            }
            else
            {
                SchedulerName schedulerName;
                Enum.TryParse(args[0], out schedulerName);

                if (schedulerName == SchedulerName.None)
                {
                    Console.WriteLine("Scheduler argument error: Not valid argument found.");
                    Log.Error("Scheduler argument error: Not valid argument found.");
                }
                else
                {
                    try
                    {
                        Console.WriteLine(string.Format("************* {0} Initialize ****************", schedulerName));
                        Log.Info(string.Format("************* {0} Initialize ****************", schedulerName));
                        Console.WriteLine(string.Format("Start Date Time {0}", DateTime.Now.ToString()));
                        Log.Info(string.Format("Start Date Time {0}", DateTime.Now.ToString()));

                        JobHandler.SchedulerJob(schedulerName);

                        Console.WriteLine(string.Format("************* {0} Completed ****************", schedulerName));
                        Log.Info(string.Format("************* {0} Completed ****************", schedulerName));
                        Console.WriteLine(string.Format("Completed Date Time {0}", DateTime.Now.ToString()));
                        Log.Info(string.Format("Completed Date Time {0}", DateTime.Now.ToString()));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(string.Format("{0} Error: {1}", schedulerName, ex.Message));
                        Log.Fatal(string.Format("{0} Error: {1}", schedulerName, ex.Message), ex);
                    }

                }

            }
            //Console.ReadKey();

        }

    }
}
