﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TurnerNotification.TurnerNotification;
using TurnerNotification.TurnerNotificationUsage;
using TurnerNotification.ScheduleDeliveryNotification;
using CommonNotification.ScheduleCompleteDelivery;

namespace TurnerNotification
{
    public class JobHandler
    {
        public static void SchedulerJob(SchedulerName schedulerName)
        {
            bool isNotImpleted = false;
            switch (schedulerName)
            {
                case SchedulerName.TurnerNotificationScheduler:
                    TurnerNotificationHelper.ProcessTurnerNotification();
                    break;

                case SchedulerName.TurnerNotificationUsage:
                    TurnerNotificationUsageHelper.ProcessTurnerNotificationUsage();
                    break;

                case SchedulerName.ScheduleDeliveryNotificationScheduler:
                    ScheduleDeliveryNotificationHelper.ProcessScheduleDeliveryNotification();
                    break;

                case SchedulerName.ScheduleCompleteDeliveryScheduler:
                    ScheduleCompleteDeliveryHelper.ProcessCompleteScheduleDelivery();
                    break;

                default:
                    isNotImpleted = true;
                    break;
            }

            if (isNotImpleted)
                throw new Exception(string.Format("{0} not implemented yet.", schedulerName));
        }

    }

    public enum SchedulerName
    {
        None = 0,
        TurnerNotificationScheduler = 1,
        TurnerNotificationUsage = 2,
        ScheduleDeliveryNotificationScheduler=3,
        ScheduleCompleteDeliveryScheduler=4
    }

}
