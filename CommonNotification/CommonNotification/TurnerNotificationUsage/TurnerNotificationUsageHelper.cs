﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using log4net;

namespace TurnerNotification.TurnerNotificationUsage
{
    public static class TurnerNotificationUsageHelper
    {
        //Declare an instance for log4net
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static void ProcessTurnerNotificationUsage()
        {

            //load, prepare, insert and upload documents
            Log.Debug("Start getting notification usage.");

            bool success = TurneNotificationUsageUtil.GetNotificationUsage();

            if (success)
                Log.Debug("Usage got successfully sent.");
            else
                Log.Debug("Get Usage failed.");

            Log.Debug("End notification usage.");
        }
    }
}
