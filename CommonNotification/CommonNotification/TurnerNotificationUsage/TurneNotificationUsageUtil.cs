﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//using TurnerNotification.CommonUtil;
using Twilio;
using Twilio.Rest.Api.V2010.Account.Usage;

namespace TurnerNotification.TurnerNotificationUsage
{
    public static class TurneNotificationUsageUtil
    {
        public static bool GetNotificationUsage()
        {
            bool successs = false;


            // Find your Account Sid and Auth Token at twilio.com/console
            //const string accountSid = "AC8805b669631fdc8684103d640cfbcd60";
            //const string authToken = "your_auth_token";

            //string accountSid = ConfigurationManager.AppSettings["twilioAccountSID"];
            //string authToken = ConfigurationManager.AppSettings["twilioAccountToken"];
            //string sendFromContact = ConfigurationManager.AppSettings["twilioActiveNumber"];

            //string usageFromDate = ConfigurationManager.AppSettings["UsageFromDate"];
            //string usageEndDate = ConfigurationManager.AppSettings["UsageEndDate"];

            TwilioAccount twilioAccount = CommonUtil.GetTwilioAccount();
            int usageDuration = Convert.ToInt32(ConfigurationManager.AppSettings["UsageDuration"]);

            TwilioClient.Init(twilioAccount.AccountSId, twilioAccount.AuthToken);

            //testing
            //UsageRecords
            //LastMonthResource.
            //TodayResource
            //DailyResource
            //RecordResource

            //testing

            TurnerNotificationUsageModel modelUsage;

            for(int i= usageDuration;  i>=1; i--)
            {
                DateTime start_date = DateTime.Today.AddDays(-i);
                DateTime end_date = DateTime.Today.AddDays(-i);

               //sms inbound
               var records_inbound = RecordResource.Read(
               category: RecordResource.CategoryEnum.SmsInbound,
               startDate: Convert.ToDateTime(start_date),
               endDate: Convert.ToDateTime(end_date)
               );


                foreach (var record in records_inbound)
                {
                    //Console.WriteLine(record.Price);

                    modelUsage = new TurnerNotificationUsageModel();

                    modelUsage.Category = record.Category.ToString();
                    modelUsage.Count = record.Count;
                    modelUsage.CountUnit = record.CountUnit;
                    modelUsage.Description = record.Description;
                    modelUsage.StartDate = record.StartDate.HasValue? record.StartDate.Value : start_date;
                    modelUsage.EndDate = record.EndDate.HasValue ? record.EndDate.Value : end_date;
                    modelUsage.Usage = record.Usage;
                    modelUsage.UsageUnit = record.UsageUnit;

                    TurnerNotificationUsageSQLHelper.InsertDeliverySmsCount(modelUsage);
                }

                //sms out bound
                var records_outbound = RecordResource.Read(
                category: RecordResource.CategoryEnum.SmsOutbound,
                startDate: Convert.ToDateTime(start_date),
                endDate: Convert.ToDateTime(end_date)
                );


                foreach (var record in records_outbound)
                {
                    //Console.WriteLine(record.Price);

                    modelUsage = new TurnerNotificationUsageModel();

                    modelUsage.Category = record.Category.ToString();
                    modelUsage.Count = record.Count;
                    modelUsage.CountUnit = record.CountUnit;
                    modelUsage.Description = record.Description;
                    modelUsage.StartDate = record.StartDate.HasValue ? record.StartDate.Value : start_date;
                    modelUsage.EndDate = record.EndDate.HasValue ? record.EndDate.Value : end_date;
                    modelUsage.Usage = record.Usage;
                    modelUsage.UsageUnit = record.UsageUnit;

                    TurnerNotificationUsageSQLHelper.InsertDeliverySmsCount(modelUsage);
                }

            }

           


            return successs;
        }

     }
}
