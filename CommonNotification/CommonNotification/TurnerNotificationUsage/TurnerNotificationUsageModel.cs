﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnerNotification.TurnerNotificationUsage
{
    public class TurnerNotificationUsageModel
    {
        public string Category;
        public string Count;
        public string CountUnit;
        public string Description;
        public DateTime StartDate;
        public DateTime EndDate;
        public string Usage;
        public string UsageUnit;
        public int Id;
        
    }
}
