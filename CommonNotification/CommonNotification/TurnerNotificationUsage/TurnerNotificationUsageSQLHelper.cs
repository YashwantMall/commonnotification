﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Xml.Linq;

namespace TurnerNotification.TurnerNotificationUsage
{
    public static class TurnerNotificationUsageSQLHelper
    {
        static string conn = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        public static string InsertDeliverySmsCount(TurnerNotificationUsageModel modelUsage)
        {
            string result = "";
            SqlConnection con = new SqlConnection(conn);
            try
            {
                using (con)
                {
                    SqlCommand cmd = new SqlCommand();
                    con.Open();
                    cmd.CommandText = "InsertDelivery_SMS_Count";
                    cmd.Parameters.Add("@category", SqlDbType.NVarChar).Value = modelUsage.Category;
                    cmd.Parameters.Add("@count", SqlDbType.NVarChar).Value = modelUsage.Count;
                    cmd.Parameters.Add("@count_unit", SqlDbType.NVarChar).Value = modelUsage.CountUnit;
                    cmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = modelUsage.Description;
                    cmd.Parameters.Add("@start_date", SqlDbType.NVarChar).Value = modelUsage.StartDate;
                    cmd.Parameters.Add("@end_date", SqlDbType.NVarChar).Value = modelUsage.EndDate;
                    cmd.Parameters.Add("@usage", SqlDbType.NVarChar).Value = modelUsage.Usage;
                    cmd.Parameters.Add("@usage_unit", SqlDbType.NVarChar).Value = modelUsage.UsageUnit;
   
                    SqlParameter sqlParam = new SqlParameter("@out_id", SqlDbType.NVarChar);
                    sqlParam.Value = "";
                    sqlParam.Size = 200;
                    sqlParam.Direction = ParameterDirection.Output;

                    sqlParam = cmd.Parameters.Add(sqlParam);


                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.CommandTimeout = 4000;
                    int res = cmd.ExecuteNonQuery();
                    result = sqlParam.Value.ToString();
                }
            }
            catch (Exception ex)
            {
                con.Close();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

    }

}
