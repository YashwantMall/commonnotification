﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using log4net;

namespace TurnerNotification.TurnerNotification
{
    public class TurnerNotificationHelper
    {
        //Declare an instance for log4net
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// process
        /// </summary>
        public static void ProcessTurnerNotification()
        {

            //load, prepare, insert and upload documents
            Log.Debug("Start sms notification.");

            bool success = TurnerNotificationUtil.SendNotification();

            if(success)
                Log.Debug("SMS successfully sent.");
            else
                Log.Debug("Send sms failed.");

            Log.Debug("End sms notification.");
        }

    }
}
