﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Xml.Linq;

namespace TurnerNotification.TurnerNotification
{
    public static class TurnerNotificationSQLHelper
    {
        static string conn = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        public static DataTable GetNotifictionDetails(int notificatonCounter)
        {
            DataTable dt = new DataTable();

            SqlConnection con = new SqlConnection(conn);
            try
            {
                using (con)
                {
                    SqlCommand cmd = new SqlCommand();
                    con.Open();
                    cmd.CommandText = "GetNotificationDetails";
                    cmd.Parameters.Add("@days", SqlDbType.NVarChar).Value = notificatonCounter;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.CommandTimeout = 6000;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);


                }
            }
            catch (Exception ex)
            {
                string e = ex.Message;
                con.Close();
            }
            finally
            {
                con.Close();
            }
            return dt;
        }

        public static string InsertDeliverySmsStatus(OrderDeliveryDetails orderDeliveryDetails)
        {
            string result = "";
            SqlConnection con = new SqlConnection(conn);
            try
            {
                using (con)
                {
                    SqlCommand cmd = new SqlCommand();
                    con.Open();
                    cmd.CommandText = "InsertDelivery_SMS_Status";
                    cmd.Parameters.Add("@ordernumber", SqlDbType.NVarChar).Value = orderDeliveryDetails.OrderNo;
                    cmd.Parameters.Add("@deliverydate", SqlDbType.NVarChar).Value = orderDeliveryDetails.DeliveryDate;
                    cmd.Parameters.Add("@saledate", SqlDbType.NVarChar).Value = orderDeliveryDetails.SaleDate;
                    cmd.Parameters.Add("@customerid", SqlDbType.NVarChar).Value = orderDeliveryDetails.CustomerId;
                    cmd.Parameters.Add("@contact_number", SqlDbType.NVarChar).Value = orderDeliveryDetails.ContactNo;
                    cmd.Parameters.Add("@contact_number2", SqlDbType.NVarChar).Value = orderDeliveryDetails.ContactNo2;
                    cmd.Parameters.Add("@templateid", SqlDbType.NVarChar).Value = orderDeliveryDetails.TemplateId;
                    cmd.Parameters.Add("@deliverycode", SqlDbType.NVarChar).Value = orderDeliveryDetails.DeliveryCode;
                    cmd.Parameters.Add("@send_status", SqlDbType.NVarChar).Value = orderDeliveryDetails.Status;
                    cmd.Parameters.Add("@send_status2", SqlDbType.NVarChar).Value = orderDeliveryDetails.Status2;

                    cmd.Parameters.Add("@company_id", SqlDbType.NVarChar).Value = orderDeliveryDetails.CompanyId;
                    cmd.Parameters.Add("@store_id", SqlDbType.NVarChar).Value = orderDeliveryDetails.StoreId;

                    SqlParameter sqlParam = new SqlParameter("@statusid", SqlDbType.NVarChar);
                    sqlParam.Value = "";
                    sqlParam.Size = 200;
                    sqlParam.Direction = ParameterDirection.Output;

                    sqlParam = cmd.Parameters.Add(sqlParam);


                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.CommandTimeout = 4000;
                    int res = cmd.ExecuteNonQuery();
                    result = sqlParam.Value.ToString();
                }
            }
            catch (Exception ex)
            {
                con.Close();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        //send sms entries to call log table

        public static string InsertDeliverySMSNotification(OrderDeliveryDetails orderDeliveryDetails)
        {
            string result = "";
            SqlConnection con = new SqlConnection(conn);
            try
            {
                using (con)
                {
                    SqlCommand cmd = new SqlCommand();
                    con.Open();
                    cmd.CommandText = "InsertDeliverySMSNotification";
                    cmd.Parameters.Add("@ordernumber", SqlDbType.NVarChar).Value = orderDeliveryDetails.OrderNo;
                    cmd.Parameters.Add("@deliverydate", SqlDbType.NVarChar).Value = orderDeliveryDetails.DeliveryDate;
                    cmd.Parameters.Add("@customerid", SqlDbType.NVarChar).Value = orderDeliveryDetails.CustomerId;
                    cmd.Parameters.Add("@contact_number1", SqlDbType.NVarChar).Value = orderDeliveryDetails.ContactNo;
                    cmd.Parameters.Add("@contact_number2", SqlDbType.NVarChar).Value = orderDeliveryDetails.ContactNo2;
                    cmd.Parameters.Add("@emailid", SqlDbType.NVarChar).Value = orderDeliveryDetails.Email;
                    cmd.Parameters.Add("@purpose", SqlDbType.NVarChar).Value = orderDeliveryDetails.Purpose;
                    cmd.Parameters.Add("@type", SqlDbType.NVarChar).Value = orderDeliveryDetails.Type;
                    cmd.Parameters.Add("@send_status1", SqlDbType.NVarChar).Value = orderDeliveryDetails.Status;
                    cmd.Parameters.Add("@send_status2", SqlDbType.NVarChar).Value = orderDeliveryDetails.Status2;

                    cmd.Parameters.Add("@company_id", SqlDbType.NVarChar).Value = orderDeliveryDetails.CompanyId;
                    cmd.Parameters.Add("@store_id", SqlDbType.NVarChar).Value = orderDeliveryDetails.StoreId;
                    cmd.Parameters.Add("@create_by", SqlDbType.NVarChar).Value = orderDeliveryDetails.CreatedBy;
                    cmd.Parameters.Add("@template_id", SqlDbType.NVarChar).Value = orderDeliveryDetails.TemplateId;
                    cmd.Parameters.Add("@message", SqlDbType.NVarChar).Value = orderDeliveryDetails.Message;
                    
                    SqlParameter sqlParam = new SqlParameter("@id", SqlDbType.NVarChar);
                    sqlParam.Value = "";
                    sqlParam.Size = 200;
                    sqlParam.Direction = ParameterDirection.Output;

                    sqlParam = cmd.Parameters.Add(sqlParam);


                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.CommandTimeout = 4000;
                    int res = cmd.ExecuteNonQuery();
                    result = sqlParam.Value.ToString();
                }
            }
            catch (Exception ex)
            {
                con.Close();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static string UpdateDeliverySMSNotification(OrderDeliveryDetails orderDeliveryDetails)
        {
            string result = "";
            SqlConnection con = new SqlConnection(conn);
            try
            {
                using (con)
                {
                    SqlCommand cmd = new SqlCommand();
                    con.Open();
                    cmd.CommandText = "UpdateDeliverySMSNotification";
                    cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = orderDeliveryDetails.Id;
                    cmd.Parameters.Add("@status1", SqlDbType.NVarChar).Value = orderDeliveryDetails.Status;
                    cmd.Parameters.Add("@status2", SqlDbType.NVarChar).Value = orderDeliveryDetails.Status2;
                    cmd.Parameters.Add("@message", SqlDbType.NVarChar).Value = orderDeliveryDetails.Message;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.CommandTimeout = 4000;
                    int res = cmd.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                con.Close();
            }
            finally
            {
                con.Close();
            }
            return result;
        }
    }
}
