﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnerNotification.TurnerNotification
{
    public class TurnerNotificationModel
    {
        public int TemplateCounter { get; set; }
        public string Template { get; set; }
        public int NotificationCount { get; set; }

    }

    public class OrderDeliveryDetails
    {

        public string OrderNo;
        public string DeliveryDate;
        public string SaleDate;
        public string CustomerId;
        public string ContactNo;
        public string ContactNo2;
        public string TemplateId;
        public string DeliveryCode;
        public string DurationDay;
        public string Message;
        public string Status;
        public string Status2;
        public string Id;
        public string CompanyId;
        public string StoreId;
        public string Email;

        public string Purpose;
        public string Type;
        public string CreatedBy;
        public string AvailableDeliveryDate;



    }
}
