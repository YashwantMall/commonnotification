﻿using CommonNotification.CommonUtil;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace TurnerNotification.TurnerNotification
{
    public static class TurnerNotificationUtil
    {
        //Declare an instance for log4net
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static bool SendNotification()
        {
            List<TurnerNotificationModel> lstNotification = new List<TurnerNotificationModel>();
            lstNotification.Add(new TurnerNotificationModel() { TemplateCounter = 0, NotificationCount = 0, Template = "10+" });
            lstNotification.Add(new TurnerNotificationModel() { TemplateCounter = 1, NotificationCount = 0, Template = "20+" });
            lstNotification.Add(new TurnerNotificationModel() { TemplateCounter = 2, NotificationCount = 0, Template = "30+" });
            lstNotification.Add(new TurnerNotificationModel() { TemplateCounter = 3, NotificationCount = 0, Template = "40+" });
            lstNotification.Add(new TurnerNotificationModel() { TemplateCounter = 4, NotificationCount = 0, Template = "50+" });
            lstNotification.Add(new TurnerNotificationModel() { TemplateCounter = 5, NotificationCount = 0, Template = "60+" });
            lstNotification.Add(new TurnerNotificationModel() { TemplateCounter = 6, NotificationCount = 0, Template = "70+" });

            //List<int> lstNotificationCounter = new List<int>() { 0, 1, 2, 3, 4, 5, 6 };
            List<int> lstNotificationCounter = new List<int>() { 6, 5, 4, 3, 2, 1, 0 };

            foreach (int counter in lstNotificationCounter)
            {
                Console.WriteLine("");
                Console.WriteLine("Sending Notfication for template:" + lstNotification.FirstOrDefault(x => x.TemplateCounter == counter).Template);
                Log.Info("Sending Notfication for template:" + lstNotification.FirstOrDefault(x => x.TemplateCounter == counter).Template);

                handleSendNotification(counter, lstNotification);

                Console.WriteLine("Send Notfication completed for template:" + lstNotification.FirstOrDefault(x => x.TemplateCounter == counter).Template);
                Log.Info("Send Notfication completed for template:" + lstNotification.FirstOrDefault(x => x.TemplateCounter == counter).Template);

            }

            Log.Info("Sending mail...");
            //send notificaton mail
            sendMail(lstNotification);
            Log.Info("Send mail completed.");

            return true;
        }

       private static bool handleSendNotification(int notificatonCounter, List<TurnerNotificationModel> lstNotification)
       {
            bool successs = false;
            //get notification infomation
            DataTable dt = new DataTable();
            OrderDeliveryDetails orderDeliveryDetails = new OrderDeliveryDetails();

            int counter = 0;
            int smsCounter = 0;
            try
            {
                dt = TurnerNotificationSQLHelper.GetNotifictionDetails(notificatonCounter);

                if (dt.Rows.Count > 0)
                {
                    //check sms count
                    var result = dt.AsEnumerable().Where(dr => dr.Field<int>("level") == 0);
                    DataTable dt1 = result.CopyToDataTable();
                    DataRow[] rows = dt1.Select();

                    lstNotification.FirstOrDefault(x => x.TemplateCounter == notificatonCounter).NotificationCount = rows.Count();
                    Console.WriteLine("count of customer to notify:" + rows.Count());
                    Log.Info("count of customer to notify:" + rows.Count());

                    foreach (DataRow row in dt.Rows)
                    {
                        //if (counter >= 2)
                        //    break;

                        //pause 2 min 
                        if(smsCounter>500)
                        {
                            System.Threading.Thread.Sleep(2 * 60 * 1000);
                            smsCounter = 0;
                        }

                        int level = Convert.ToInt32(row["level"].ToString());

                        if(level==0)
                        {
                            orderDeliveryDetails = new OrderDeliveryDetails();

                            //
                            orderDeliveryDetails.OrderNo = row["OrderNumber"] == null ? string.Empty : row["OrderNumber"].ToString();
                            orderDeliveryDetails.DeliveryDate = row["DeliveryDate"] == null ? string.Empty : row["DeliveryDate"].ToString();
                            orderDeliveryDetails.SaleDate = row["SalesDate"] == null ? string.Empty : row["SalesDate"].ToString();
                            orderDeliveryDetails.CompanyId = row["company_id"] == null ? string.Empty : row["company_id"].ToString();
                            orderDeliveryDetails.StoreId = row["store_id"] == null ? string.Empty : row["store_id"].ToString();
                            //
                            orderDeliveryDetails.CustomerId = row["CustID"].ToString();
                            orderDeliveryDetails.ContactNo = row["ContactNo"].ToString().Trim();
                            orderDeliveryDetails.ContactNo2 = row["ContactNo2"].ToString().Trim();
                            orderDeliveryDetails.Message = row["Message"].ToString();
                            orderDeliveryDetails.TemplateId = lstNotification.FirstOrDefault(x => x.TemplateCounter == notificatonCounter).Template; // row["TemplateId"].ToString();

                            //
                            orderDeliveryDetails.Purpose = "Delivery Notification";
                            orderDeliveryDetails.Type = "SMS";
                            orderDeliveryDetails.CreatedBy = "Delivery Notification Service";
                            //
                            //if (orderDeliveryDetails.TemplateId.Trim() == "1")
                            //{
                            //    orderDeliveryDetails.Message = orderDeliveryDetails.Message + "\nhttps://youtu.be/yMnkE4AYCsc";
                            //}

                            //orderDeliveryDetails.ContactNo = "7038999630"; //"7038999630";// "+919860645687";
                            //orderDeliveryDetails.ContactNo2 = "5404195311";// "2292244765";// "+917276092022";

                            string id = string.Empty;
                            id = TurnerNotificationSQLHelper.InsertDeliverySMSNotification(orderDeliveryDetails);
                            if (string.IsNullOrWhiteSpace(id))
                                continue;

                            orderDeliveryDetails.Id = id;

                            //sms - contact1
                            if (!string.IsNullOrEmpty(orderDeliveryDetails.ContactNo))
                            {
                                try
                                {
                                    orderDeliveryDetails.ContactNo = string.Format("+1{0}", orderDeliveryDetails.ContactNo);

                                    successs = SMSUtil.SendSMSNotification(orderDeliveryDetails.ContactNo, orderDeliveryDetails.Message);
                                    smsCounter++;
                                    if (successs)
                                        orderDeliveryDetails.Status = "Success";
                                    else
                                        orderDeliveryDetails.Status = "Failed";

                                }
                                catch (Exception ex)
                                {
                                    Log.Error(string.Format("Failed - {0}", ex.Message));
                                    if (ex.Message.Contains("SMS queue is full"))
                                        successs = handleSmsQueueFullError(orderDeliveryDetails);
                                    else
                                    {
                                        successs = false;
                                        orderDeliveryDetails.Status = string.Format("Failed - {0}", ex.Message);
                                    }
                                }

                            }

                            //sms - contact2
                            if (!string.IsNullOrEmpty(orderDeliveryDetails.ContactNo2))
                            {
                                try
                                {
                                    orderDeliveryDetails.ContactNo2 = string.Format("+1{0}", orderDeliveryDetails.ContactNo2);
                                    successs = SMSUtil.SendSMSNotification(orderDeliveryDetails.ContactNo2, orderDeliveryDetails.Message);
                                    smsCounter++;

                                    if (successs)
                                        orderDeliveryDetails.Status2 = "Success";
                                    else
                                        orderDeliveryDetails.Status2 = "Failed";

                                }
                                catch (Exception ex)
                                {
                                    Log.Error(string.Format("Failed - {0}", ex.Message));
                                    if (ex.Message.Contains("SMS queue is full"))
                                        successs = handleSmsQueueFullError(orderDeliveryDetails);
                                    else
                                    {
                                        successs = false;
                                        orderDeliveryDetails.Status2 = string.Format("Failed - {0}", ex.Message);
                                    }                                   
                                }
                            }

                            //update  delivery notification                           
                            TurnerNotificationSQLHelper.UpdateDeliverySMSNotification(orderDeliveryDetails);
                        }
                        else if(level == 1)
                        {
                            orderDeliveryDetails.OrderNo = row["OrderNumber"].ToString();
                            orderDeliveryDetails.DeliveryDate = row["DeliveryDate"].ToString();
                            orderDeliveryDetails.SaleDate = row["SalesDate"].ToString();
                            orderDeliveryDetails.CustomerId = row["CustID"].ToString();
                            orderDeliveryDetails.ContactNo = row["ContactNo"].ToString().Trim();
                            orderDeliveryDetails.ContactNo2 = row["ContactNo2"].ToString().Trim();
                            orderDeliveryDetails.Message = row["Message"].ToString();
                            orderDeliveryDetails.TemplateId = row["TemplateId"].ToString();
                            orderDeliveryDetails.DeliveryCode = row["so_dtl_del_via_cod"].ToString();

                            orderDeliveryDetails.CompanyId = row["company_id"].ToString();
                            orderDeliveryDetails.StoreId = row["store_id"].ToString();

                            //insert InsertDeliverySmsStatus ---it is not related to sms but to manage data for notification
                            TurnerNotificationSQLHelper.InsertDeliverySmsStatus(orderDeliveryDetails);
                        }

                        counter++;
                    }
                }


            }
            catch(Exception ex)
            {
                Log.Error("Exception - " + ex.Message);
                Console.WriteLine("Exception -" + ex.Message);
            }

            return successs;
       }

        private static bool handleSmsQueueFullError(OrderDeliveryDetails orderDeliveryDetails)
        {
            bool successs = false;
            try
            {
                Log.Info("send message again failed due to sms queue full.");
                System.Threading.Thread.Sleep(10 * 60 * 1000);

                successs = SMSUtil.SendSMSNotification(orderDeliveryDetails.ContactNo2, orderDeliveryDetails.Message);
                
                if (successs)
                    orderDeliveryDetails.Status2 = "Success";
                else
                    orderDeliveryDetails.Status2 = "Failed";

            }
            catch (Exception ex)
            {
                successs = false;
                orderDeliveryDetails.Status2 = string.Format("Failed - {0}", ex.Message);
                Log.Error(string.Format("Failed - {0}", ex.Message));
            }

            return successs;

        }


        private static bool sendMail(List<TurnerNotificationModel> lstNotification)
        {
            bool status = false;
            try
            {
                string result = string.Empty;
                //if (!string.IsNullOrEmpty(orderDeliveryDetails.EmailId))
                {
                    ////log mail to db
                    //orderDeliveryDetails.Purpose = "Signature";
                    //orderDeliveryDetails.Type = "Email";
                    //id = sqlh.InsertCalllogDelivery(orderDeliveryDetails);

                    string emailIds = "mstephens@ahs-se.com,amitesh@iconnectgroup.com,ymall@iconnectgroup.com";
                    //emailIds = "ymall@iconnectgroup.com,msharma@iconnectgroup.com";

                    //send mail
                    string subject = string.Format("Notification sms sent on date - {0}", DateTime.Now.Date);
                    //prepare body
                    string bodyEmail = "Below are the details for sent notification \n\n";
                    foreach(TurnerNotificationModel notification in lstNotification)
                    {
                        bodyEmail += string.Format("\nNotification Type: {0}  => Notification Count: {1} ", notification.Template, notification.NotificationCount);
                        bodyEmail += "\n";
                    }

                    bodyEmail += "\n\n\n\nThanks,\niConnect Group Team";

                   result = EmailUtil.UsingAmazoneSendMail(subject, emailIds, bodyEmail);

                    if (result == "success")
                    {
                        status = true;
                        Log.Info("Send mail status - success");

                    }
                        
                }


            }
            catch (Exception ex)
            {
                status = false;
                Log.Error("Exceptton" + ex.Message);
            }

            return status;
        }
    }
}
