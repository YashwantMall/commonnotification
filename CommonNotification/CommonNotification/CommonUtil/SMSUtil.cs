﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace TurnerNotification
{
    public class SMSUtil
    {
        public static bool SendSMSNotification(string sendToContact, string strBody)
        {
            // Find your Account Sid and Token at twilio.com/console
            // DANGER! This is insecure. See http://twil.io/secure
            //string accountSid = string.Empty;// ConfigurationManager.AppSettings["twilioAccountSID"];
            //string authToken = string.Empty;// ConfigurationManager.AppSettings["twilioAccountToken"];
            //string sendFromContact = string.Empty;// ConfigurationManager.AppSettings["twilioActiveNumber"];

            //
            TwilioAccount twilioAccount = CommonUtil.GetTwilioAccount();

            TwilioClient.Init(twilioAccount.AccountSId, twilioAccount.AuthToken);

            var message = MessageResource.Create(
                body: strBody,
                from: new Twilio.Types.PhoneNumber(twilioAccount.PhoneNumber),
                to: new Twilio.Types.PhoneNumber(sendToContact)
            );


            if (message.ErrorCode != null)
                return false;
            else
                return true;
        }
    }
}
