﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace TurnerNotification
{
    public static class CommonUtil
    {
        public static TwilioAccount GetTwilioAccount()
        {
            TwilioAccount account = new TwilioAccount();
            //using xml
            string clientCode = ConfigurationManager.AppSettings["client_app"];
            XmlDocument doc = new XmlDocument();
            doc.Load(Directory.GetCurrentDirectory() + "//TwilioAccounts.xml");
            XmlNodeList elms = doc.SelectNodes("/clients/client[@name='" + clientCode.ToLower() + "']");
            XmlNodeList lstAccount = elms.Item(0).SelectNodes("twilio");
            foreach (XmlNode node in lstAccount)
            {
                account.AccountSId = node.Attributes["account_sid"].Value;
                account.AuthToken = node.Attributes["auth_token"].Value;
                account.PhoneNumber = node.Attributes["phone_number"].Value;
            }
            //

            return account;
        }
    }

    public class TwilioAccount
    {
        public string AccountSId { get; set; }
        public string AuthToken { get; set; }
        public string PhoneNumber { get; set; }
    }
}
