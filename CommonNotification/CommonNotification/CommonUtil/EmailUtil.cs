﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Configuration;

namespace CommonNotification.CommonUtil
{
    public static class EmailUtil
    {

        public static string UsingAmazoneSendMail(string sub, string toemail, string body, bool isHtmlBoday=false)
        {
            //, List<tbl_email_template_attachment> attachment
            string mailstatus = string.Empty;
            string FROM = ConfigurationManager.AppSettings["fromMail"];
            string TO = toemail;

            string reployEmail = ConfigurationManager.AppSettings["reployMail"];
            string displayName = ConfigurationManager.AppSettings["displayMail"];
            string ccMail = ConfigurationManager.AppSettings["ccMail"];
            string bccMail = ConfigurationManager.AppSettings["bccMail"];

            string SUBJECT = sub;
            string SMTP_USERNAME = ConfigurationManager.AppSettings["smtpUser"];  // Replace with your SMTP username. 
            string SMTP_PASSWORD = ConfigurationManager.AppSettings["smtpPassword"];  // Replace with your SMTP password.

            string HOST = ConfigurationManager.AppSettings["smtpHost"];
            int PORT = Convert.ToInt32(ConfigurationManager.AppSettings["smptPort"]);//already tried with all recommended ports

            using (System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(HOST, PORT))
            {
                // Create a network credential with your SMTP user name and password.
                client.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);

                // Use SSL when accessing Amazon SES. The SMTP session will begin on an unencrypted connection, and then 
                // the client will issue a STARTTLS command to upgrade to an encrypted connection using SSL.
                client.EnableSsl = true;
                var emailMessage = new System.Net.Mail.MailMessage(FROM, TO)
                {
                    IsBodyHtml = isHtmlBoday,
                    Subject = sub,
                    Body = body,
                };
                emailMessage.ReplyToList.Add(new MailAddress(reployEmail, "reply-to"));
                emailMessage.From = new MailAddress(FROM, displayName);
                
                if(!string.IsNullOrEmpty(ccMail))
                    emailMessage.CC.Add(ccMail);

                if (!string.IsNullOrEmpty(bccMail))
                    emailMessage.Bcc.Add(bccMail);

                //new
                /*foreach (var attachmentfilename in attachment)
                {
                    if (attachmentfilename.attachment.ToString() != string.Empty)
                    {

                        string path = HttpContext.Current.Server.MapPath("~/assets/Attachment/" + attachmentfilename.attachment.ToString());
                        if (File.Exists(path))
                        {
                            Attachment attachment1 = new Attachment(path, System.Net.Mime.MediaTypeNames.Application.Octet);
                            System.Net.Mime.ContentDisposition disposition = attachment1.ContentDisposition;
                            disposition.CreationDate = File.GetCreationTime(path);
                            disposition.ModificationDate = File.GetLastWriteTime(path);
                            disposition.ReadDate = File.GetLastAccessTime(path);
                            disposition.FileName = Path.GetFileName(path);
                            disposition.Size = new FileInfo(path).Length;
                            disposition.DispositionType = System.Net.Mime.DispositionTypeNames.Attachment;
                            emailMessage.Attachments.Add(attachment1);
                        }
                    }
                }*/


                // Send the email. 
                try
                {
                    client.Send(emailMessage);
                    mailstatus = "success";
                }
                catch (Exception ex)
                {
                    //_Comman.LogWrite(Enums.LogType.Error, ex.Message.ToString(), "Utilis", "UsingAmazoneSendMail");
                    mailstatus = "fail";
                }
            }
            return mailstatus;


        }

        public static string UsingYahooSendMail(string sub, string toemail, string body)
        {
            //, List<tbl_email_template_attachment> attachment
            string mailstatus = string.Empty;
            string FROM = ConfigurationManager.AppSettings["fromMail"];
            string TO = toemail;

            string reployEmail = ConfigurationManager.AppSettings["reployMail"];
            string displayName = ConfigurationManager.AppSettings["displayMail"];

            string SUBJECT = sub;
            string SMTP_USERNAME = "customerservice@afhstx.com";// ConfigurationManager.AppSettings["smtpUser"];  // Replace with your SMTP username. 
            string SMTP_PASSWORD = "Jasmine2020@";// ConfigurationManager.AppSettings["smtpPassword"];  // Replace with your SMTP password.

            string HOST = "smtp.bizmail.yahoo.com";// "imap.mail.yahoo.com";// "plus.smtp.mail.yahoo.com";// "smtp.mail.yahoo.com";// "smtp.bizmail.yahoo.com";// ConfigurationManager.AppSettings["smtpHost"];
            int PORT = 465;// 993;// 465;// Convert.ToInt32(ConfigurationManager.AppSettings["smptPort"]);//already tried with all recommended ports

            using (System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(HOST, PORT))
            {
                // Create a network credential with your SMTP user name and password.
                client.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);

                // Use SSL when accessing Amazon SES. The SMTP session will begin on an unencrypted connection, and then 
                // the client will issue a STARTTLS command to upgrade to an encrypted connection using SSL.
                client.EnableSsl = true;
                var emailMessage = new System.Net.Mail.MailMessage(FROM, TO)
                {
                    IsBodyHtml = true,
                    Subject = sub,
                    Body = body,
                };
                emailMessage.ReplyToList.Add(new MailAddress(reployEmail, "reply-to"));
                emailMessage.From = new MailAddress(FROM, displayName);

                // Send the email. 
                try
                {
                    client.Send(emailMessage);
                    mailstatus = "success";
                }
                catch (Exception ex)
                {
                    //_Comman.LogWrite(Enums.LogType.Error, ex.Message.ToString(), "Utilis", "UsingAmazoneSendMail");
                    mailstatus = "fail";
                }
            }

            ///////////
            /*using (System.Net.Mail.SmtpClient client1 = new System.Net.Mail.SmtpClient("smtp.mail.yahoo.com", 587))
            {
                // Create a network credential with your SMTP user name and password.
                client1.Credentials = new System.Net.NetworkCredential("customerservice@afhstx.com", "Jasmine2020@");
                // Use SSL when accessing Amazon SES. The SMTP session will begin on an unencrypted connection, and then
                // the client will issue a STARTTLS command to upgrade to an encrypted connection using SSL.
                client1.EnableSsl = true;
                var emailMessage = new System.Net.Mail.MailMessage("customerservice@afhstx.com", "amanrajvanshi@yahoo.com")
                {
                    IsBodyHtml = true,
                    Subject = "Subject",
                    Body = "Main Message",
                };
                emailMessage.ReplyToList.Add(new MailAddress("amanrajvanshi@yahoo.com", "reply-to"));
                emailMessage.From = new MailAddress("customerservice@afhstx.com", "CustomerCare");
                try
                {
                    client1.Send(emailMessage);
                    //mailstatus = "success";
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                    // mailstatus = "fail";
                }
            }*/

            /////////////
            return mailstatus;


        }

    }
}
