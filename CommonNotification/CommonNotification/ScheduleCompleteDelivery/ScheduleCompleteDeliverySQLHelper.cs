﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Xml.Linq;

namespace CommonNotification.ScheduleCompleteDelivery
{
    public static class ScheduleCompleteDeliverySQLHelper
    {
        static string conn = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        static string connCRM = ConfigurationManager.ConnectionStrings["CRMConnectionString"].ConnectionString;

        public static DataTable GetSMSTemplate()
        {
            DataTable dt = new DataTable();

            SqlConnection conCRM = new SqlConnection(connCRM);
            try
            {
                using (conCRM)
                {
                    SqlCommand cmd = new SqlCommand();
                    conCRM.Open();
                    cmd.CommandText = "getSMSTemplateList";

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conCRM;
                    cmd.CommandTimeout = 6000;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);

                }
            }
            catch (Exception ex)
            {
                string e = ex.Message;
                conCRM.Close();
            }
            finally
            {
                conCRM.Close();
            }
            return dt;
        }

        public static DataTable GetCompletedNotifictionDetails()
        {
            DataTable dt = new DataTable();

            SqlConnection con = new SqlConnection(conn);
            try
            {
                using (con)
                {
                    SqlCommand cmd = new SqlCommand();
                    con.Open();
                    cmd.CommandText = "Proc_GetCompleteDeliveryDataToSendSMS";
                    //cmd.Parameters.Add("@Company_ID", SqlDbType.NVarChar).Value = "-1";
                    //cmd.Parameters.Add("@profitcenter_id", SqlDbType.NVarChar).Value = "-1";
                    //cmd.Parameters.Add("@from_date", SqlDbType.NVarChar).Value = "";
                    //cmd.Parameters.Add("@to_date", SqlDbType.NVarChar).Value = "";
                    //cmd.Parameters.Add("@jobflag", SqlDbType.NVarChar).Value = "0";
                    //cmd.Parameters.Add("@exclude", SqlDbType.NVarChar).Value = "1";

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.CommandTimeout = 6000;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);


                }
            }
            catch (Exception ex)
            {
                string e = ex.Message;
                con.Close();
            }
            finally
            {
                con.Close();
            }
            return dt;
        }

        public static string InsertScheduleDeliverySmsStatus(ScheduleCompleteDeliveryModel orderDeliveryDetails)
        {
            string result = "";
            SqlConnection con = new SqlConnection(conn);
            try
            {
                using (con)
                {
                    SqlCommand cmd = new SqlCommand();
                    con.Open();
                    cmd.CommandText = "InsertCalllogScheduleDelivery";
                    cmd.Parameters.Add("@ordernumber", SqlDbType.NVarChar).Value = orderDeliveryDetails.OrderNo;
                    cmd.Parameters.Add("@deliverydate", SqlDbType.NVarChar).Value = orderDeliveryDetails.DeliveryDate;
                    //cmd.Parameters.Add("@saledate", SqlDbType.NVarChar).Value = orderDeliveryDetails.SaleDate;
                    cmd.Parameters.Add("@customerid", SqlDbType.NVarChar).Value = orderDeliveryDetails.CustomerId;
                    cmd.Parameters.Add("@contact_number1", SqlDbType.NVarChar).Value = orderDeliveryDetails.ContactNo1;
                    cmd.Parameters.Add("@contact_number2", SqlDbType.NVarChar).Value = orderDeliveryDetails.ContactNo2;
                    cmd.Parameters.Add("@emailid", SqlDbType.NVarChar).Value = orderDeliveryDetails.Email;
                    cmd.Parameters.Add("@purpose", SqlDbType.NVarChar).Value = orderDeliveryDetails.Purpose;
                    cmd.Parameters.Add("@type", SqlDbType.NVarChar).Value = orderDeliveryDetails.Type;
                    cmd.Parameters.Add("@send_status1", SqlDbType.NVarChar).Value = orderDeliveryDetails.Status1;
                    cmd.Parameters.Add("@send_status2", SqlDbType.NVarChar).Value = orderDeliveryDetails.Status2;

                    cmd.Parameters.Add("@company_id", SqlDbType.NVarChar).Value = orderDeliveryDetails.CompanyId;
                    cmd.Parameters.Add("@store_id", SqlDbType.NVarChar).Value = orderDeliveryDetails.StoreId;
                    cmd.Parameters.Add("@create_by", SqlDbType.NVarChar).Value = orderDeliveryDetails.CreatedBy;
                    cmd.Parameters.Add("@template_id", SqlDbType.NVarChar).Value = orderDeliveryDetails.TemplateId;
                    cmd.Parameters.Add("@message", SqlDbType.NVarChar).Value = orderDeliveryDetails.Message;
                    //cmd.Parameters.Add("@avail_del_date", SqlDbType.NVarChar).Value = orderDeliveryDetails.AvailableDeliveryDate;

                    SqlParameter sqlParam = new SqlParameter("@calllogid", SqlDbType.NVarChar);
                    sqlParam.Value = "";
                    sqlParam.Size = 200;
                    sqlParam.Direction = ParameterDirection.Output;

                    sqlParam = cmd.Parameters.Add(sqlParam);


                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.CommandTimeout = 4000;
                    int res = cmd.ExecuteNonQuery();
                    result = sqlParam.Value.ToString();
                }
            }
            catch (Exception ex)
            {
                con.Close();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

        public static string UpdateCalllogDelivery(ScheduleCompleteDeliveryModel orderDeliveryDetails)
        {
            string result = "";
            SqlConnection con = new SqlConnection(conn);
            try
            {
                using (con)
                {
                    SqlCommand cmd = new SqlCommand();
                    con.Open();
                    cmd.CommandText = "UpdateCalllogScheduleDelivery";
                    cmd.Parameters.Add("@calllogid", SqlDbType.NVarChar).Value = orderDeliveryDetails.Id;
                    cmd.Parameters.Add("@status1", SqlDbType.NVarChar).Value = orderDeliveryDetails.Status1;
                    cmd.Parameters.Add("@status2", SqlDbType.NVarChar).Value = orderDeliveryDetails.Status2;
                    cmd.Parameters.Add("@message", SqlDbType.NVarChar).Value = orderDeliveryDetails.Message;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.CommandTimeout = 4000;
                    int res = cmd.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                con.Close();
            }
            finally
            {
                con.Close();
            }
            return result;
        }

    }
}
