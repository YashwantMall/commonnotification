﻿using CommonNotification.CommonUtil;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace CommonNotification.ScheduleCompleteDelivery
{
    public static class ScheduleCompleteDeliveryUtil
    {
        //Declare an instance for log4net
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static bool SenNotification()
        {
            bool successs = false;
            //get notification infomation
            DataTable dt = new DataTable();
            ScheduleCompleteDeliveryModel orderDeliveryDetails = new ScheduleCompleteDeliveryModel();

            int counter = 0;
            try
            {
                Log.Info("Send Notification started...");
                string client_app = ConfigurationManager.AppSettings["client_app"];
                string notification_jobtype = string.Empty;
                notification_jobtype = ConfigurationManager.AppSettings["notification_jobtype"];
                List<NotificationModel> lstNotification = new List<NotificationModel>();
                NotificationModel notification = new NotificationModel();

                dt = ScheduleCompleteDeliverySQLHelper.GetCompletedNotifictionDetails();

                if (dt.Rows.Count > 0)
                {

                    string purpose = string.Empty;
                    string createdby = string.Empty;

                    string url = ConfigurationManager.AppSettings["redirecturl"];
                    string key_link = string.Empty;
                    string body = string.Empty;
                    string templateId = string.Empty;
                    DataRow[] rows = dt.Select();

                    if (client_app == "turner")
                    {
                        #region turner
                        if (notification_jobtype == "1")//completed
                        {
                            var result = dt.AsEnumerable().Where(dr => dr.Field<string>("Items In Stock") == "In Stock");
                            DataTable dt1 = result.CopyToDataTable();
                            rows = dt1.Select();
                            ///

                            purpose = "Complete Delivery";
                            createdby = "Complete Delivery Service";
                            key_link = ConfigurationManager.AppSettings["key_trcdel"];

                            DataTable dtTemplate = ScheduleCompleteDeliverySQLHelper.GetSMSTemplate();
                            DataRow row = dtTemplate.AsEnumerable().SingleOrDefault(r => r.Field<string>("Templatename") == "Complete Delivery");
                            if (row != null)
                            {
                                body = row["templatemessage"].ToString();
                                templateId = row["id"].ToString();
                            }
                            else
                                body = "Hi, this is your Ashley HomeStore.  \nGood news, we are ready to schedule you for delivery of your purchase!  Please click here to confirm your delivery date.  ";
                        }

                        #endregion

                    }

                    url = string.Format("{0}/{1}/", url, key_link);

                    if (rows != null)
                    {
                        Log.Info("Total customer found: " + rows.Count());

                        foreach (DataRow row in rows)
                        {

                            // if(row["so_no"].ToString().Contains(','))
                            {
                                if (counter >= 1)
                                    break;

                                orderDeliveryDetails = new ScheduleCompleteDeliveryModel();
                                notification = new NotificationModel();

                                orderDeliveryDetails.Purpose = purpose;
                                orderDeliveryDetails.CreatedBy = createdby;
                                orderDeliveryDetails.Type = "SMS";

                                if (client_app == "turner")
                                {
                                    #region turner
                                    orderDeliveryDetails.OrderNo = row["Sales Number"].ToString().Trim();
                                    orderDeliveryDetails.CustomerId = row["Cust_id"].ToString();
                                    orderDeliveryDetails.Email = null;
                                    orderDeliveryDetails.DeliveryDate = row["deliverydate"].ToString();
                                    orderDeliveryDetails.CompanyId = row["company_id"].ToString();
                                    orderDeliveryDetails.StoreId = row["profitcenter_id"].ToString();
                                    orderDeliveryDetails.CustomerName = row["name"].ToString();
                                    orderDeliveryDetails.TemplateId = templateId;

                                    string phone = row["phone 1"].ToString();
                                    string[] arrphone = phone.Split(',');
                                    if (arrphone != null && arrphone.Length > 1)
                                    {
                                        orderDeliveryDetails.ContactNo1 = arrphone[0].Trim();
                                        orderDeliveryDetails.ContactNo2 = arrphone[1].Trim();
                                    }
                                    else
                                    {
                                        orderDeliveryDetails.ContactNo1 = phone.Trim();
                                        orderDeliveryDetails.ContactNo2 = "";
                                    }
                                    #endregion

                                }

                                string id = string.Empty;
                                id = ScheduleCompleteDeliverySQLHelper.InsertScheduleDeliverySmsStatus(orderDeliveryDetails);
                                if (string.IsNullOrWhiteSpace(id))
                                    continue;

                                orderDeliveryDetails.Id = id;

                                orderDeliveryDetails.Message = body.Replace("#Link#", string.Format("{0}{1}", url, id));

                                #region send sms
                                //sms - contact1
                                if (!string.IsNullOrWhiteSpace(orderDeliveryDetails.ContactNo1))
                                {
                                    try
                                    {
                                        orderDeliveryDetails.ContactNo1 = string.Format("+1{0}", orderDeliveryDetails.ContactNo1.Trim());

                                        successs = TurnerNotification.SMSUtil.SendSMSNotification(orderDeliveryDetails.ContactNo1, orderDeliveryDetails.Message);

                                        if (successs)
                                            orderDeliveryDetails.Status1 = "Success";
                                        else
                                            orderDeliveryDetails.Status1 = "Failed";

                                    }
                                    catch (Exception ex)
                                    {
                                        successs = false;
                                        orderDeliveryDetails.Status1 = string.Format("Failed - {0}", ex.Message);
                                        Log.Error(string.Format("Failed - {0}", ex.Message));
                                    }

                                }

                                //sms - contact2
                                if (!string.IsNullOrWhiteSpace(orderDeliveryDetails.ContactNo2))
                                {
                                    try
                                    {
                                        orderDeliveryDetails.ContactNo2 = string.Format("+1{0}", orderDeliveryDetails.ContactNo2.Trim());
                                        successs = TurnerNotification.SMSUtil.SendSMSNotification(orderDeliveryDetails.ContactNo2, orderDeliveryDetails.Message);

                                        if (successs)
                                            orderDeliveryDetails.Status2 = "Success";
                                        else
                                            orderDeliveryDetails.Status2 = "Failed";

                                    }
                                    catch (Exception ex)
                                    {
                                        successs = false;
                                        orderDeliveryDetails.Status2 = string.Format("Failed - {0}", ex.Message);
                                        Log.Error(string.Format("Failed - {0}", ex.Message));
                                    }
                                }
                                #endregion

                                notification.CustomerId = orderDeliveryDetails.CustomerId;
                                notification.CustomerName = orderDeliveryDetails.CustomerName;
                                if (orderDeliveryDetails.Status1 == "Success" || orderDeliveryDetails.Status2 == "Success")
                                    notification.NotificationStatus = orderDeliveryDetails.Status1;
                                else if (!string.IsNullOrEmpty(orderDeliveryDetails.Status1))
                                    notification.NotificationStatus = orderDeliveryDetails.Status1;
                                else
                                    notification.NotificationStatus = orderDeliveryDetails.Status2;


                                //update InsertDeliverySmsStatus  
                                if (string.IsNullOrWhiteSpace(orderDeliveryDetails.ContactNo1) && string.IsNullOrWhiteSpace(orderDeliveryDetails.ContactNo2))
                                    orderDeliveryDetails.Status1 = "Failed - No any contact number available.";

                                ScheduleCompleteDeliverySQLHelper.UpdateCalllogDelivery(orderDeliveryDetails);
                                //counter++;
                                lstNotification.Add(notification);
                            }

                        }

                    }

                }

                //send mail
                sendMail(lstNotification);

            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Failed - {0}", ex.Message));
            }

            return successs;
        }

        private static bool sendMail(List<NotificationModel> lstNotification)
        {
            bool status = false;
            try
            {
                Log.Info("Sending mail...");
                string result = string.Empty;
                //if (!string.IsNullOrEmpty(orderDeliveryDetails.EmailId))
                {

                    string emailIds = ConfigurationManager.AppSettings["toMail"];
                    //emailIds = "ymall@iconnectgroup.com,msharma@iconnectgroup.com";

                    //send mail
                    string subject = string.Format("SMS sent - {0}", DateTime.Now);
                    //prepare body
                    string bodyEmail = string.Empty;
                    bodyEmail += "<table ><tr><td colspan='2'>";
                    bodyEmail += "SMS Sent";
                    bodyEmail += "</td></tr>";
                    bodyEmail += string.Format("<tr><td colspan='2'>Complete Deliveries - {0} </td></tr> ", lstNotification.Count);

                    bodyEmail += "<tr><td colspan='2'></br></br></td></tr> ";
                    bodyEmail += "<tr><td colspan='2'></br></br></td></tr> ";
                    bodyEmail += "<tr><td colspan='2'></br></br></td></tr> ";
                    bodyEmail += "<tr><td colspan='2'></br></br></td></tr> ";
                    bodyEmail += "<tr><td colspan='2'>Thanks,</br></td></tr> ";
                    bodyEmail += "<tr><td colspan='2'></br>iConnect Support Team</td></tr> ";
                    bodyEmail += "</table>";

                    result = EmailUtil.UsingAmazoneSendMail(subject, emailIds, bodyEmail, true);

                    if (result == "success")
                    {
                        status = true;
                        Log.Info("Send mail status - success");

                    }

                }


            }
            catch (Exception ex)
            {
                status = false;
                Log.Error("Exceptton" + ex.Message);
            }

            return status;
        }
    }
}
