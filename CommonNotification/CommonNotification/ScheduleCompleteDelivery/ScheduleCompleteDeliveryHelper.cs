﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using log4net;

namespace CommonNotification.ScheduleCompleteDelivery
{
    public class ScheduleCompleteDeliveryHelper
    {
        //Declare an instance for log4net
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// process
        /// </summary>
        public static void ProcessCompleteScheduleDelivery()
        {

            //load, prepare, insert and upload documents
            Log.Debug("Start sms notification.");

            bool success = ScheduleCompleteDeliveryUtil.SenNotification();

            if (success)
                Log.Debug("SMS successfully sent.");
            else
                Log.Debug("Send sms failed.");

            Log.Debug("End sms notification.");
        }
    }
}
