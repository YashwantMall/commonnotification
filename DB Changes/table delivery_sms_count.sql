
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[delivery_sms_count](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[category] varchar(100) not null,
	[count] decimal(20,5) not null,
	[count_unit] varchar(100) not null,
	[description] varchar(250) null,
	[start_date] datetime  null,
	[end_date] datetime null,
	[usage] decimal(20,5) NOT NULL,
	[usage_unit] varchar(100) null,
	[create_date] datetime not null,
	[update_date] datetime null
	
) ON [PRIMARY]
GO

--drop table delivery_sms_count
