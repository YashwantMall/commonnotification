

/****** Object:  Table [dbo].[CustomerDeliveryDateConfirmation]    Script Date: 11/18/2020 2:04:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CustomerDeliveryDateConfirmation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OrderNo] [varchar](1000) NULL,
	[DeliveryDate] [datetime] NULL,
	[Cust_Name] [varchar](100) NULL,
	[Cust_Address] [varchar](1000) NULL,
	[ConfirmedDeliveryDate] [datetime] NULL,
	[Locations] [varchar](1000) NULL,
	[ConfirmationDate_Changed] [varchar](1) NULL,
	[Insert_Date] [datetime] NULL,
	[CustID] [varchar](50) NULL,
	[ItemNos] [varchar](1000) NULL,
	[SaleDate] [datetime] NULL,
	[RemoveFlag] [varchar](1) NULL,
	[ITEMS] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO