

/****** Object:  Table [dbo].[Deliveryzips]    Script Date: 11/19/2020 4:51:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Deliveryzips](
	[deliveryzipsID] [int] NULL,
	[CityName] [nvarchar](50) NULL,
	[Days] [nvarchar](50) NULL,
	[Fee] [numeric](18, 2) NULL
) ON [PRIMARY]
GO

