
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[delivery_sms_status](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order_no] varchar(100) not null,
	[delivery_date] datetime not null,
	[sale_date] datetime not null,
	[cust_id] varchar(250) not null,
	[contact_no] varchar(50)  null,
	[contact_no2] varchar(50) null,
	[template_id] [int] NOT NULL,
	[delivery_code] varchar(10) null,
	[send_status] varchar(1000)  null,
	[send_status2] varchar(1000)  null,
	[last_updaated_date] datetime not null
	
) ON [PRIMARY]
GO

--drop table delivery_sms_status
