
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[delivery_sms_template](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[duration_day] [int] NOT NULL,
	[message] [varchar](5000) NOT NULL,
	[active] [bit]  default 1
	
) ON [PRIMARY]
GO


