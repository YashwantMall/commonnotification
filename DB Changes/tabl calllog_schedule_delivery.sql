
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[calllog_schedule_delivery](
	[calllog_id] [int] IDENTITY(1,1) NOT NULL,
	[company_id] [int] NULL,
	[store_id] [int] NULL,
	[ordernumber] [varchar](1000) NULL,
	[deliverydate] [datetime] NULL,
	[customerid] [varchar](50) NULL,
	[contact_number1] [varchar](50) NULL,
	[contact_number2] [varchar](50) NULL,
	[emailid] [varchar](100) NULL,
	[purpose] [varchar](50) NULL,
	[type] [varchar](50) NULL,
	[template_id] int,
	[message] nvarchar(max),
	[status1] [varchar](1000) NULL,
	[status2] [varchar](1000) NULL,
	[created_by] [varchar](50) NULL,
	[created_date] [datetime] NULL
	
) ON [PRIMARY]
GO


--drop table calllog_schedule_delivery