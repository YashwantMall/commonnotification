insert into delivery_sms_template (duration_day, message) values
(10, 'Hi, this is Ashley HomeStore.  We just wanted to touch base with you and thank you for your patience.  We know you purchased a while ago and we just wanted to let you know that we are monitoring your ticket closely.  As soon as your items have arrived at our local facility, we will contact you to get you scheduled for delivery or pickup.  Should you need anything, feel free to text back.  Thank you!')

insert into delivery_sms_template (duration_day, message) values
(20, 'Hi, this is Ashley HomeStore.  We are still monitoring your items and are will contact you as soon as your items have arrived so we can get you scheduled for a delivery or pick-up.  If you have any questions, feel free to text us back or call (877)484-5577.')

insert into delivery_sms_template (duration_day, message) values
(30, 'Hello, this is Ashley HomeStore.  We are just touching base to let you know we are still watching your order.  Hopefully, we will be able to get you scheduled for delivery or pick-up soon.  We really appreciate your patience!  If there is anything you need in the meantime, feel free to text us back or call (877)484-5577.')

insert into delivery_sms_template (duration_day, message) values
(40, 'Hi, Ashley HomeStore again.  We hope that we will receive some merchandise soon for your delivery/pickup.  We know that the manufacturer is working non-stop to catch up with the demand.  We are still checking your order every single day and as soon as we have some merchandise for delivery or pickup we will contact you ASAP to set a date! Thanks for being so patient with us!  Feel free to text us back or call (877)484-5577 if we can help in any way.')


GetNotificationDetails
InsertDelivery_SMS_Status
InsertDelivery_SMS_Count